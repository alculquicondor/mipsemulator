#include "microcompiler.h"

MicroCompiler::MicroCompiler()
{

}

MicroCompiler *MicroCompiler::pinstance = nullptr;
const MicroCompiler &MicroCompiler::getInstance()
{
    if (pinstance == nullptr)
        pinstance = new MicroCompiler;
    return *pinstance;
}

vector<word> MicroCompiler::compile(word instr) const
{
    word op, fn, tmp;
    vector <word> micro;

    micro.push_back(6815765);
    micro.push_back(536576);

    op = (instr &(63 << 26)) >> 26;
    if(op == 0) // R
    {
        fn = (instr &(63 << 0)) >> 0;
        if (fn == 8) { // jr
            micro.push_back(2097153);
        } else {
            tmp = 6144;
            tmp += fn << 14;
            micro.push_back(tmp);
            micro.push_back(1344);
        }
    }
    else if (op == 15 or op == 8 or op == 10
             or (op >= 12 and op <= 14)) // ALU imm
    {
        tmp = 10240;
        switch (op) {
        case 8: tmp += 32 << 14; break;
        case 10: tmp += 42 << 14; break;
        case 12: tmp += 36 << 14; break;
        case 13: tmp += 37 << 14; break;
        case 14: tmp += 38 << 14; break;
        }
        micro.push_back(tmp);
        micro.push_back(1280);
    }
    else if (op == 2) // j
    {
        micro.push_back(1);
    }
    else if (op == 35 or op == 43)
    {
        micro.push_back(534528);
        if (op == 43) { // store
            micro.push_back(10);
        } else { // load
            micro.push_back(6);
            micro.push_back(1024);
        }
    }
    else if (op == 1 or op == 4 or op == 5) //branch
    {
        tmp = 4757504;
        switch (op) {
        case 1: tmp += 1 << 23; break;
        case 4: tmp += 2 << 23; break;
        case 5: tmp += 3 << 23; break;
        }
        micro.push_back(tmp);
    }
    return micro;
}
