#ifndef REGISTERFILE_H
#define REGISTERFILE_H
#include "mainheader.h"

class RegisterFile : public QObject
{
    Q_OBJECT
private:
    word file[32];

public:
    RegisterFile();
    virtual ~RegisterFile();

    word loadWord(byte address);
    void writeWord(byte address, word content);
    void refresh();

signals:
    void registersChanged(const vector<word> &values);

};

#endif // REGISTERFILE_H
