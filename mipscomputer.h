#ifndef MIPSCOMPUTER_H
#define MIPSCOMPUTER_H
#include "mainheader.h"
#include "memcache.h"
#include "registerfile.h"
#include "alu.h"
#include "microcompiler.h"

class MIPScomputer
{
private:
    MemCache *memcache;
    RegisterFile *registerfile;
    ALU *alu;
    word pc, instReg, dataReg, xReg, yReg, zReg;
    vector<word> instruction;
    void runInstruction(word instr);
public:
    MIPScomputer();
    friend class MainWindow;
    word get_pc();
    void run(const vector<word> &instruction);
    void runBySteps(const vector<word> &instruction);
    bool step();
    void abort();
};

#endif // MIPSCOMPUTER_H
