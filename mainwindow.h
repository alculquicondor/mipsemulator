#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "mipscomputer.h"
#include <QMainWindow>
#include <QStandardItemModel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    MIPScomputer *computer;
    vector<word> get_instructions_ui();
signals:
    void statusMsg(const QString &msg, int timeout = 0);
private slots:
    void updateRegisters(const vector<word> &values);
    void updateMemory(const vector<pair<word, byte>> &values);
    void on_runOnceBtn_clicked();
    void on_byStepsBtn_clicked();
    void on_stepBtn_clicked();
    void on_abortBtn_clicked();
};

#endif // MAINWINDOW_H
