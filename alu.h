#ifndef ALU_H
#define ALU_H
#include "mainheader.h"

class ALU
{
public:
    ALU();
    word operate(byte func, word x, word y);
    byte zero, ovfl;
};

#endif // ALU_H
