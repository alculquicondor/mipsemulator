#ifndef MEMCACHE_H
#define MEMCACHE_H
#include "mainheader.h"
#include <unordered_map>

using std::unordered_map;
typedef unordered_map<word, byte> umwb;

class MemCache : public QObject
{
    Q_OBJECT
private:
    umwb memo;
public:
    MemCache();
    virtual ~MemCache();
    word loadWord(word address);
    void writeWord(word address, word content);
    void refresh();
    void clear();
signals:
    void memoryChanged(const vector<pair<word, byte>> &values);
};

#endif // MEMCACHE_H
