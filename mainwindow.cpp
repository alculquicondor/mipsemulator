#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(this, SIGNAL(statusMsg(QString,int)), ui->statusBar, SLOT(showMessage(QString,int)));
    // hide buttons
    ui->stepBtn->setVisible(false);
    ui->abortBtn->setVisible(false);
    computer = new MIPScomputer();
    // model for registers
    QStandardItemModel *model = new QStandardItemModel(32, 2, this);
    model->setHorizontalHeaderItem(0, new QStandardItem("address"));
    model->setHorizontalHeaderItem(1, new QStandardItem("word"));
    ui->registerTable->setModel(model);
    // model for memory
    model = new QStandardItemModel(0, 2, this);
    model->setHorizontalHeaderItem(0, new QStandardItem("address"));
    model->setHorizontalHeaderItem(1, new QStandardItem("byte"));
    ui->memoryTable->setModel(model);
    // connections and update
    connect(computer->registerfile, SIGNAL(registersChanged(vector<word>)),
            SLOT(updateRegisters(vector<word>)));
    computer->registerfile->refresh();
    connect(computer->memcache, SIGNAL(memoryChanged(vector<pair<word,byte> >)),
            SLOT(updateMemory(vector<pair<word,byte> >)));
    computer->memcache->refresh();
}

MainWindow::~MainWindow()
{
    delete ui;
}

vector<word> MainWindow::get_instructions_ui()
{
    vector<word> instr;
    QString text = ui->instructionsEdit->toPlainText();
    word tmp;
    for (const QString &line : text.split("\n")) {
        tmp = 0;
        if (line.size() != 32) {
            emit statusMsg("Instrucción inválida", 2000);
            return vector<word> ();
        }
        for (int i = 0; i < 32; ++i) {
            if (line[i] != '0' and line[i] != '1') {
                emit statusMsg("Instrucción inválida", 2000);
                return vector<word> ();
            }
            tmp = (tmp<<1) + line[i].digitValue();
        }
        instr.push_back(tmp);
    }
    return instr;
}

void MainWindow::updateRegisters(const vector<word> &values)
{
    QStandardItemModel *model = (QStandardItemModel *)ui->registerTable->model();
    QStandardItem *item;
    for (int row = 0; row <32; row++) {
        item = new QStandardItem(QString::number(row, 16));
        model->setItem(row, 0, item);
        item = new QStandardItem(QString::number(values[row], 16));
        model->setItem(row, 1, item);
    }
}

void MainWindow::updateMemory(const vector<pair<word, byte> > &values)
{
    QStandardItemModel *model = (QStandardItemModel *)ui->memoryTable->model();
    QStandardItem *item;
    int row = 0;
    for (auto &p : values) {
        item = new QStandardItem(QString::number(p.first, 16));
        model->setItem(row, 0, item);
        item = new QStandardItem(QString::number(p.second, 16));
        model->setItem(row, 1, item);
        ++row;
    }
}

void MainWindow::on_runOnceBtn_clicked()
{
    ui->byStepsBtn->setEnabled(false);
    computer->run(get_instructions_ui());
    ui->byStepsBtn->setEnabled(true);
}

void MainWindow::on_byStepsBtn_clicked()
{
    ui->byStepsBtn->setVisible(false);
    ui->runOnceBtn->setVisible(false);
    ui->stepBtn->setVisible(true);
    ui->abortBtn->setVisible(true);
    computer->runBySteps(get_instructions_ui());
}

void MainWindow::on_stepBtn_clicked()
{
    emit statusMsg(QString("Instruccion %1 en ejecución")
                   .arg(computer->get_pc()>>2));
    if (not computer->step()) {
        ui->byStepsBtn->setVisible(true);
        ui->runOnceBtn->setVisible(true);
        ui->stepBtn->setVisible(false);
        ui->abortBtn->setVisible(false);
    }
}

void MainWindow::on_abortBtn_clicked()
{
    computer->abort();
    ui->byStepsBtn->setVisible(true);
    ui->runOnceBtn->setVisible(true);
    ui->stepBtn->setVisible(false);
    ui->abortBtn->setVisible(false);
}
