# Emulador de MIPS

##Integrantes
- Aldo Culquicondor
- Harry Marquez

###TODO
- `MainWindow`: obtener las instrucciones desde el campo de texto y
convertirlas a `word`
- `MicroCompiler`: obtener las microinstrucciones para cada instrucción
- `MIPScomputer`: Ejecutar las instrucciones usando las microinstrucciones
- `ALU`: Ejecutar operaciones ALU
