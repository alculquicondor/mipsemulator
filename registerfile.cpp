#include "registerfile.h"

RegisterFile::RegisterFile()
{
    file[0] = 0;
}

RegisterFile::~RegisterFile()
{
}

word RegisterFile::loadWord(byte address)
{
    return file[address];
}

void RegisterFile::writeWord(byte address, word content)
{
     file[address] = content;
     vector<word> values(file, file+32);
     emit registersChanged(values);
}

void RegisterFile::refresh()
{
     vector<word> values(file, file+32);
     emit registersChanged(values);
}
