#include "mipscomputer.h"

void MIPScomputer::runInstruction(word instr)
{
    vector<word> micro = MicroCompiler::getInstance().compile(instr);
    for (word u : micro) {
        // --- MemCache ---
        if (u & (1<<2)) { // MemRead
            if (u & (1<<1)) // InstData
                dataReg = memcache->loadWord(zReg);
            else
                dataReg = instruction[pc>>2];
        }
        if (u & (1<<4)) // IRwrite
            instReg = dataReg;
        if (u & (1<<3)) // MemWrite
            memcache->writeWord(zReg, yReg);
        // --- RegFile ---
        xReg = registerfile->loadWord((instReg & (31 << 21)) >> 21);
        yReg = registerfile->loadWord((instReg & (31 << 16)) >> 16);
        if (u & (1 << 10)) { // RegWrite
            byte dest;
            word data;
            word tmp = (u & (3<<6)) >> 6;
            switch ((u & (3 << 6)) >> 6) { // RegDst
                case 0: dest = (instReg & (31 << 16)) >> 16; break;
                case 1: dest = (instReg & (31 << 11)) >> 11; break;
                case 2: dest = 31;
            }
            switch ((u & (3 << 8)) >> 8) { // RegInSrc
                case 0: data = dataReg; break;
                case 1: data = zReg; break;
                case 2: data = pc; break;
            }
            registerfile->writeWord(dest, data);
        }
        // --- ALU ---
        word x, y, alu_out;
        if (u & (1<<11)) // ALUSrcX
            x = xReg;
        else
            x = pc;
        switch ((u & (3 << 12)) >> 12) { // ALUSrcY
            case 0: y = 4; break;
            case 1: y = yReg; break;
            case 2: y = (int)((short)instReg); break; // imm
            case 3: y = (int)((short)instReg) << 2; break; // imm*4
        }
        alu_out = alu->operate((u & (63<<14))>>14, x, y);
        // branch
        switch ((u & (3<<23)) >> 23) {
        case 1: u |= (alu_out & (1<<31)) >> 31; break;
        case 2: u |= alu->zero; break;
        case 3: u |= not alu->zero; break;
        }
        // --- PCWrite ---
        if (u & 1) {
            switch ((u & (3<<21)) >> 21) {
                case 0: pc = ((u & (1<<20)) ? // JumpAddr
                                  0x4000000 : (instReg & 0x3ffffff)) << 2; break;
                case 1: pc = xReg; break;
                case 2: pc = zReg; break;
                case 3: pc = alu_out; break;
            }
        }
        zReg = alu_out;
    }
}

MIPScomputer::MIPScomputer()
{
    memcache = new MemCache;
    registerfile = new RegisterFile;
    alu = new ALU;
}

word MIPScomputer::get_pc()
{
    return pc;
}

void MIPScomputer::run(const vector<word> &instruction)
{
    pc = 0;
    this->instruction = instruction;
    while ((pc>>2) < instruction.size())
        runInstruction(instruction[pc>>2]);
}

void MIPScomputer::runBySteps(const vector<word> &instruction)
{
    this->instruction = instruction;
    pc = 0;
}

bool MIPScomputer::step()
{
    if ((pc>>2) == instruction.size())
        return false;
    runInstruction(instruction[pc>>2]);
    return (pc>>2) < instruction.size();
}

void MIPScomputer::abort()
{
    pc = 0;
    memcache->clear();
}
