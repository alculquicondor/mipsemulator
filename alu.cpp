#include "alu.h"

ALU::ALU()
{
}

word ALU::operate(byte func, word x, word y)
{
    word ans;
    switch (func) {
    case 0: ans = y << 16; break;
    case 32: ans = x + y; break;
    case 34: ans = x - y; break;
    case 36: ans = x & y; break;
    case 37: ans = x | y; break;
    case 38: ans = x ^ y; break;
    case 39: ans = ~ (x | y); break;
    case 42: ans = x < y; break;
    }
    ovfl = 0;
    if (func == 32)
        ovfl = (x & (1u<<31)) == (y & (1u<<31)) and (x & (1u<<31)) != (ans & (1u<<31));
    zero = ans == 0;
    return ans;
}
