#include "memcache.h"

MemCache::MemCache()
{
    memo.reserve(1024*1024);
}

MemCache::~MemCache()
{
    memo.clear();
}

word MemCache::loadWord(word address)
{
    word ans = 0;
    for (int i = 0; i < 4; ++i)
        ans = (ans << 8) + memo[address+i];
    return ans;
}

void MemCache::writeWord(word address, word content)
{
    for (int i = 3; i >= 0; --i) {
        memo[address+i] = (byte)content;
        content >>= 8;
    }
    vector<pair<word, byte>> values(memo.begin(), memo.end());
    emit memoryChanged(values);
}

void MemCache::refresh()
{
    vector<pair<word, byte>> values(memo.begin(), memo.end());
    emit memoryChanged(values);
}

void MemCache::clear()
{
    memo.clear();
}
