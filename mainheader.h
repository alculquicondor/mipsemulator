#ifndef MAINHEADER_H
#define MAINHEADER_H
#include <QObject>
#include <vector>

typedef unsigned char byte; // 8 bits
typedef unsigned int word; // 32 bits
using std::vector;
using std::pair;

#endif // MAINHEADER_H
