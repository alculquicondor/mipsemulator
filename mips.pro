#-------------------------------------------------
#
# Project created by QtCreator 2013-11-07T11:17:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mips
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++11


SOURCES += main.cpp\
        mainwindow.cpp \
    memcache.cpp \
    mipscomputer.cpp \
    registerfile.cpp \
    alu.cpp \
    microcompiler.cpp

HEADERS  += mainwindow.h \
    memcache.h \
    mainheader.h \
    mipscomputer.h \
    registerfile.h \
    alu.h \
    microcompiler.h

FORMS    += mainwindow.ui
