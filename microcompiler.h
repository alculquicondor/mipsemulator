#ifndef MICROCOMPILER_H
#define MICROCOMPILER_H
#include "mainheader.h"

class MicroCompiler
{
private:
    MicroCompiler();
    static MicroCompiler *pinstance;
public:
    static MicroCompiler const &getInstance();
    vector<word> compile(word instr) const;
};

#endif // MICROCOMPILER_H
